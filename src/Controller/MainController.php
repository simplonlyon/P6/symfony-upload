<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Product;
use App\Form\ProductType;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;

class MainController extends Controller
{
    /**
     * @Route("/", name="main")
     */
    public function index(Request $request, FileUploader $fileUploader)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $product->getBrochure();
            $fileName = $fileUploader->upload($file);

            return $this->render('main/show.html.twig', [
                'fileURI' => $this->getParameter('brochures_URI') . $fileName
            ]);

        }
        
        return $this->render('main/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
